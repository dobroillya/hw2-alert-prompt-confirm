let age;
let name = "";
let notValidName = true;
let notValidAge = true;
let defaultValue = "";

while (notValidName) {
  name = prompt("What is your name?");
  if (name.length) {
    notValidName = false;
  } else {
    alert("Empty name");
  }
}

while (notValidAge) {
  age = prompt("How old are you?", `${defaultValue}`);
  defaultValue = age;
  if (typeof +age === "number" && age >= 0) {
    notValidAge = false;
  } else {
    alert("Not vallid age");
  }
}

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  const confirmEnter = confirm("Are you sure you want to continue?");
  if (confirmEnter) {
    alert(`Welcome, ${name}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${name}`);
}
